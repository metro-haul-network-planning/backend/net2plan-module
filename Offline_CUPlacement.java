import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.jom.DoubleMatrixND;
import com.jom.OptimizationProblem;
import com.net2plan.interfaces.networkDesign.Configuration;
import com.net2plan.interfaces.networkDesign.IAlgorithm;
import com.net2plan.interfaces.networkDesign.Net2PlanException;
import com.net2plan.interfaces.networkDesign.NetPlan;
import com.net2plan.niw.ImportMetroNetwork;
import com.net2plan.niw.OpticalSpectrumManager;
import com.net2plan.niw.WAbstractNetworkElement;
import com.net2plan.niw.WFiber;
import com.net2plan.niw.WIpLink;
import com.net2plan.niw.WIpSourceRoutedConnection;
import com.net2plan.niw.WIpUnicastDemand;
import com.net2plan.niw.WLightpath;
import com.net2plan.niw.WLightpathRequest;
import com.net2plan.niw.WNet;
import com.net2plan.niw.WNode;
import com.net2plan.niw.WServiceChain;
import com.net2plan.niw.WServiceChainRequest;
import com.net2plan.niw.WVnfInstance;
import com.net2plan.niw.WVnfType;
import com.net2plan.utils.InputParameter;
import com.net2plan.utils.Pair;
import com.net2plan.utils.Quintuple;
import com.net2plan.utils.Triple;

import cern.colt.matrix.tdouble.DoubleFactory1D;
import cern.colt.matrix.tdouble.DoubleFactory2D;
import cern.colt.matrix.tdouble.DoubleMatrix1D;
import cern.colt.matrix.tdouble.DoubleMatrix2D;

public class Offline_CUPlacement implements IAlgorithm
{
	private static final int K = 3;
	private static final int SLOTS_LIGHTPATH = 1;
	private static final double LINERATE = 100;
	private static final double SLOTS_FIBER = 8;
	private static final double idlePowerServer = 130;
	private static final double maxPowerServer = idlePowerServer/0.15;
	private static final double capexServer = 1;
	private static final double powerTransponder = 160;
	private static final double capexTransponder = 5;
	
	private InputParameter filePath						= new InputParameter ("filePath","","Path to excel file");
	private InputParameter trafficSlot 						= new InputParameter ("trafficSlot", 0, "Traffic slot to be analyzed.");
	private InputParameter optimizationType 				= new InputParameter ("optimizationType","#select# ILP ILP_nodes CRAN DRAN", "Objective function");

	@Override
	public String executeAlgorithm(NetPlan netPlan, Map<String, String> algorithmParameters, Map<String, String> net2planParameters)
	{
		/* Initialize all InputParameter objects defined in this object (this uses Java reflection) */
		InputParameter.initializeAllInputParameterFieldsOfObject(this, algorithmParameters);
		
		
			WNet wNet = new WNet (netPlan);
			
			String excelPath = filePath.getString();
			File excelFile = new File(excelPath);

			//LZ: load topology and traffic information from excel file 
			wNet = ImportMetroNetwork.importFromExcelFile(excelFile);
			//we have to do a trick because of copy reference thing in java
			netPlan.copyFrom(wNet.getNetPlan());
			wNet = new WNet(netPlan);
	
			Configuration.setOption("precisionFactor", new Double(1e-9).toString());
			Configuration.precisionFactor = 1e-9;

			/*************************************************************************************/
			/**************************** INITIALIZE SOME INFORMATION ****************************/
			/*************************************************************************************/
	
			/* Remove some previous information */
			for (WIpUnicastDemand d : new ArrayList<> (wNet.getIpUnicastDemands())) d.remove();
			for (WIpSourceRoutedConnection ip : new ArrayList<> (wNet.getIpSourceRoutedConnections())) ip.remove();
			for (WServiceChain sc : new ArrayList<> (wNet.getServiceChains())) sc.remove();
			for (WVnfInstance vnf : new ArrayList<> (wNet.getVnfInstances())) vnf.remove();
			
			//LZ: add lightpath requests between all nodes
			OpticalSpectrumManager opt = OpticalSpectrumManager.createFromRegularLps(wNet);		
			for (WNode node: wNet.getNodes()) {
				//LZ: add lightpath to every two nodes
				List<WNode> nodeList = wNet.getNodes();
				for(WNode node2 : nodeList) {
					if (!node.equals(node2))
						wNet.addLightpathRequest(node, node2, LINERATE, false);
				}
			}
			
			//LZ: couple link to lightpaths
			Map<Pair<WNode,WNode>,WIpLink> ipLinkMap = new HashMap<> (); //LZ: bidirectional IP link
			List<WLightpathRequest> lprList = wNet.getLightpathRequests();
			
			for (WLightpathRequest lpreq : lprList) {
				List <List<WFiber>> kpath = wNet.getKShortestWdmPath(K, lpreq.getA(), lpreq.getB(), Optional.empty());
				for (List<WFiber> candidpath : kpath) {
					//for each path you need to find an available wavelength if you find a path with an available wl just break
					Optional <SortedSet<Integer>> wl = opt.spectrumAssignment_firstFit(candidpath, SLOTS_LIGHTPATH, Optional.empty());
					//if optional has an object inside it
					if (wl.isPresent()) {
						WLightpath lp = lpreq.addLightpathUnregenerated(candidpath, wl.get(), false);
						opt.allocateOccupation(lp, candidpath, wl.get());
						//add ip link and couple it
						//LZ: lightpaths from A to B and from B to A must be connected through the same bidirectional IP link
						if(!ipLinkMap.containsKey(Pair.of(lpreq.getA(), lpreq.getB()))) {
							//LZ: if there os no IP link that link node A to node B in any direction, couple new IP link to lightpath request and save IP link from B to A
							Pair <WIpLink,WIpLink> iplink = wNet.addIpLinkBidirectional(lpreq.getA(), lpreq.getB(), LINERATE);
							lpreq.coupleToIpLink(iplink.getFirst());
							ipLinkMap.put(Pair.of(lpreq.getB(), lpreq.getA()), iplink.getSecond()); 
						}
						else {
							//LZ: if IP link connecting A to B exists, couple IP link from B to A to lightpath
							lpreq.coupleToIpLink(ipLinkMap.get(Pair.of(lpreq.getA(), lpreq.getB())));
						}
						break;
					}
				}
			}
			
			//LZ: set current offered traffic as the information of traffic intensity from excel file
			for(WServiceChainRequest scr : wNet.getServiceChainRequests()) {
				scr.setCurrentOfferedTrafficInGbps(scr.getTrafficIntensityInfo(trafficSlot.getInt()).get().getSecond());
			}
							
			/* Initialize variables */
			final int E_ip = wNet.getIpLinks().size();
			final int D = wNet.getServiceChainRequests().size();
			final int N = wNet.getNodes().size();
			final int F = wNet.getFibers().size();
			if (E_ip == 0 || D == 0) throw new Net2PlanException("This algorithm requires a topology with links and a demand set. E_ip: "+E_ip+" D: "+D);
		
			//LZ: list of nodes not connected to the core
			List<WNode> notConnectedToCore = new ArrayList<WNode>();
			for(WNode n : wNet.getNodes()) {
				if(n.isConnectedToNetworkCore()) continue;
				notConnectedToCore.add(n);
			}
			
			//LZ: nfvTypesIncluding_cpuRamHdLat = [VNF_name, CPU required, RAM required, HD required, processing latency]
			final List<Quintuple<String,Double,Double,Double,Double>> nfvTypesIncluding_cpuRamHdLat = new ArrayList<> (); 
			nfvTypesIncluding_cpuRamHdLat.add(Quintuple.of(";" , 0.0 , 0.0, 0.0 , 0.0)); // the VNF "start of service chain". Added for convenience
			nfvTypesIncluding_cpuRamHdLat.add(Quintuple.of(";;" , 0.0 , 0.0, 0.0 , 0.0)); // the VNF "end of service chain". Added for convenience
			for(WVnfType vnf : wNet.getVnfTypes()) {
				nfvTypesIncluding_cpuRamHdLat.add(Quintuple.of(vnf.getVnfTypeName(), vnf.getOccupCpu(), vnf.getOccupRamGBytes(), vnf.getOccupHdGBytes(), vnf.getProcessingTime_ms()));
			}
			
			if (nfvTypesIncluding_cpuRamHdLat.size() != nfvTypesIncluding_cpuRamHdLat.stream().map(type->type.getFirst()).collect (Collectors.toSet()).size ()) 
				throw new Net2PlanException("Type names cannot be repeated");

			final int NUM_EXTVNFYPES = nfvTypesIncluding_cpuRamHdLat.size(); //LZ: number of virtual network functions available
			final List<String> eVnfTypes_t = nfvTypesIncluding_cpuRamHdLat.stream().map(e->e.getFirst()).collect(Collectors.toCollection(ArrayList::new)); //LZ: list with name of virtual network functions available
			final Function<String,Integer> getTypeIndex = createMapIndex (nfvTypesIncluding_cpuRamHdLat.stream().map(e->e.getFirst()).collect(Collectors.toList()));
			
			
			/* Check all SCRs traverse VNF types as the ones provided */
			if (wNet.getServiceChainRequests().stream().anyMatch(d->d.getSequenceVnfTypes().size() != new HashSet<> (d.getSequenceVnfTypes()).size ())) throw new Net2PlanException ("Service chain requests cannot traverse a VNf of a given type more than once");
			for (WServiceChainRequest scr : wNet.getServiceChainRequests()) 
				if (scr.getSequenceVnfTypes().stream().anyMatch(tt->getTypeIndex.apply(tt) == null)) throw new Net2PlanException ("Service chain requests must have types among the ones defined in the input parameters");
			
			/* Create the "VirtualNodes": one for anycast origin, anycast destination, and then one for each (node,VNFExtendedtype) pair */
			final List<VirtualNode> vns = new ArrayList<> (2 + NUM_EXTVNFYPES * N); //LZ: list of virtual nodes
			final Map<String , List<VirtualNode>> type2vns = new HashMap<> (); //LZ: from VNF type, get virtual node 
			for (String t : eVnfTypes_t) type2vns.put(t, new ArrayList<> ());
			vns.add(new VirtualNode(true, vns.size())); //LZ: anycast origin
			vns.add(new VirtualNode(false, vns.size())); //LZ: anycast destination
			//LZ: to each vnf available, add the list VirtualNode with nodes --> associate each node to the vnfs in the service chain requests
			//LZ: indexType --> indicates the vnf number
			for (int indexType = 0; indexType < NUM_EXTVNFYPES ; indexType ++) {
				final String type = eVnfTypes_t.get(indexType);
				for (WNode n : wNet.getNodes()) {
					final VirtualNode vn = new VirtualNode(n, indexType, vns.size()); //LZ: create VirtualNode for node n
					vns.add(vn);
					//LZ: get valid nodes to instantiate the VNFs for CU-VNFs.
					//LZ: If in the CRAN scenario, CRAN is the only valid node for instantiation. Otherwise, use information from excel 
					final String validMetroNodesForInstantiation = type.contains(";")? wNet.getNodes().toString() : 
						(optimizationType.getString().contains("CRAN") ? wNet.getNodesConnectedToCore().toString() : notConnectedToCore.toString());
					if(validMetroNodesForInstantiation.contains(vn.getNode().getName()))
						type2vns.get(type).add(vn); //associate virtual node to the vnf type
				}
			}
			final VirtualNode anycastOrigin = vns.get(0);
			final VirtualNode anycastDestination = vns.get(1);
			final int NUMVNS = vns.size(); //LZ: number of virtual nodes
			//LZ: virtual nodes contain a repetition of the set of all nodes available in the network for each VNF in such a way that each VNF is associated to all nodes
			
			/* Create the virtual links. Anycast origin to any virtual node, any virtual node to anycast destination, all-to-all virtual nodes, even with themselves */
			final List<VirtualLink> index2vlink = new ArrayList<> (NUMVNS * NUMVNS - 2 * NUMVNS);  //LZ: get Virtual link from ILP index
			final Map<Pair<VirtualNode,VirtualNode> , VirtualLink> mapAnPair2VirtualLink = new HashMap<> (); //LZ: get Virtual link with key as pair of virtual nodes
			//LZ: for each pair of nodes in vns (can be the same node)
			for (VirtualNode vn1 : vns)
				for (VirtualNode vn2 : vns)
				{
					//LZ: if it is destination and/or origin of anycast, do nothing
					if (vn1.isAnycastDestination ()) continue;
					if (vn2.isAnycastOrigin()) continue;
					if (vn1.isAnycastOrigin() && vn2.isAnycastDestination()) continue;
					final VirtualLink e = new VirtualLink(vn1 , vn2, index2vlink.size()); //LZ: create VirtualLink between virtual node 1 and 2
					index2vlink.add(e); //LZ: add link to list
					final VirtualLink prevLink = mapAnPair2VirtualLink.put(Pair.of(vn1, vn2), e); //LZ: create VirtualLink between virtual node 1 and 2. note: index2vlink.size() increases one by one
					assert prevLink == null; //LZ: certify that link is not null 
				}
			final int NUMVLINKS = index2vlink.size(); //LZ: number of virtual links
			assert NUMVLINKS == NUMVNS * NUMVNS - 2 * NUMVNS;
			//LZ: as the virtual nodes present the nodes repeated as many times as the number of VNFs, the links vlink also present a repetition of each possible link as many times of the number of VNFs
	
			//LZ: create mapping of service chain request
			final List<WServiceChainRequest> index2scr = new ArrayList<> (wNet.getServiceChainRequests()); //LZ: get service chain request from index in ILP
			final SortedMap<WServiceChainRequest , Integer> scr2index = new TreeMap<> (); //LX: get index in ILP from service chain request
			for (WServiceChainRequest scr : index2scr) scr2index.put (scr, scr2index.size ());
			
			//LZ: create mapping of ip links
			final List<WIpLink> index2ipLink = new ArrayList<> (wNet.getIpLinks()); //LZ: get ip link from index in ILP
			final SortedMap<WIpLink , Integer> ipLink2index = new TreeMap<> (); //LX: get index in ILP from ip link
			for (WIpLink ee : index2ipLink) ipLink2index.put (ee, ipLink2index.size ());
	
			//LZ: create mapping of wnodes
			final List<WNode> index2wnode = new ArrayList<> (wNet.getNodes()); //LZ: get nodes from index in ILP
			final SortedMap<WNode , Integer> wnode2index = new TreeMap<> ();  //LZ: get index in ILP from nodes
			for (WNode ee : index2wnode) wnode2index.put (ee , wnode2index.size ());
			
			//LZ: create mapping of fiber links
			final List<WFiber> index2fiber = new ArrayList<> (wNet.getFibers()); //LZ: get ip link from index in ILP
			final SortedMap<WFiber , Integer> fiber2index = new TreeMap<> (); //LX: get index in ILP from ip link
			for (WFiber ee : index2fiber) fiber2index.put (ee, fiber2index.size ());
			
			
			/*************************************************************************************/
			/********************************** SOLVE THE MODEL **********************************/
			/*************************************************************************************/
			/* Create the optimization problem object (JOM library) */
			OptimizationProblem op = new OptimizationProblem();
	
			/* A_vn_vlink: 1 if vlink starts in node vn, -1 if it ends in vn, 0 otherwise */
			final DoubleMatrix2D A_vn_vlink = DoubleFactory2D.sparse.make (NUMVNS , NUMVLINKS);
			for (VirtualLink e : index2vlink) {
				A_vn_vlink.set(e.getA ().getIndexInIlp() , e.getIndexInIlp(), 1.0); //LZ: set 1 to node that is origin of a vlink
				A_vn_vlink.set(e.getB ().getIndexInIlp() , e.getIndexInIlp(), -1.0); //LZ: set -1 to node that is destination of a vlink
			}
			
			//LZ: P_vn_vlink: 1 if node that can host vnf is destination of vlink
			final DoubleMatrix2D P_vn_vlink = DoubleFactory2D.sparse.make (NUMVLINKS,NUMVNS);
			for (VirtualLink e : index2vlink) {
				for(String t : type2vns.keySet()) {
					List<VirtualNode> vn = type2vns.get(t);
					if(t.contains(";")) continue;
					if(vn.contains(e.getB())) P_vn_vlink.set(e.getIndexInIlp(), e.getB ().getIndexInIlp() , 1.0); //LZ: set 1 to node that is destination of a vlink
				}				
			}
			
			/* A_vn_scr: 1 if scr starts in node vn, -1 if it ends in vn, 0 otherwise */
			final DoubleMatrix2D A_vn_scr = DoubleFactory2D.sparse.make (NUMVNS , D);
			A_vn_scr.viewRow(0).assign(1.0); // all SCRs start in anycast origin
			A_vn_scr.viewRow(1).assign(-1.0); // all SCRs start in anycast destination
	
			final DoubleMatrix2D A_vn_scrOrig = DoubleFactory2D.sparse.make (NUMVNS , D);
			final DoubleMatrix2D A_vn_scrDest = DoubleFactory2D.sparse.make (NUMVNS , D);
			A_vn_scrOrig.viewRow(0).assign(1.0); // all SCRs start in anycast origin
			A_vn_scrDest.viewRow(1).assign(-1.0); // all SCRs start in anycast destination
			
			
			/* A_vlink_eip: fraction [0,Inf] of traffic in vlink, that traveses IP link eip */
			final DoubleMatrix2D A_vlink_eip = DoubleFactory2D.sparse.make (NUMVLINKS ,E_ip);
			//LZ: A_vlink_f: 1 if vlink is associated to fiber f, 0 otherwise
			final DoubleMatrix2D A_vlink_f = DoubleFactory2D.sparse.make(NUMVLINKS,F);
			final DoubleMatrix1D cap_fiber = DoubleFactory1D.dense.make(F);
			/* latencyMs_vlink: latency in ms associated to the sum of: 1) IP links of vlink, and 2) END VNF of vlink  */
			final DoubleMatrix1D latencyMs_vlink = DoubleFactory1D.dense.make (NUMVLINKS);
			final boolean trueIsMinimLatencyFalseMinimHops = false; //LZ: true is type of shortest path calculation is latency and false if shortest path following number of hops
			//LZ: get routing for all IP links with the carried traffic and maximum latency  
			final Pair<Map<Pair<WNode,WNode> , List<WIpLink>> , Map<Pair<WNode,WNode> , Double>> infoRoutingIgp = 
					getPotentialIpRoutingNormalizedCarrideTrafficAndMaxLatencyMsNoFailureState_shortestPath(wNet, trueIsMinimLatencyFalseMinimHops);
			final Map<Pair<WNode,WNode> , List<WIpLink>> nodePair2TraversedIpLinks = infoRoutingIgp.getFirst(); //LZ: routing information regarding node pair from previous step --> key (n1,n2) -> iplinks
			final Map<Pair<WNode,WNode> , Double> nodePair2WorstcaseLatencyMs = infoRoutingIgp.getSecond(); //LZ: routing information regarding latency form previous step --> key (n1,n2) -> latency
			//LZ: update latency information
			for (VirtualLink e : index2vlink) {
				final WNode a = e.getA().getNode();
				final WNode b = e.getB().getNode();
				if (a == null || b == null) continue; //LZ: if nodes are origin or destination unicast nodes, don't do anything
				final double latencyIpLinksMs;
				if (a.equals(b))
					latencyIpLinksMs = 0.0; //LZ: if vlink origin and destination is the same node, latency is null
				else {
					for (WIpLink ipLink : nodePair2TraversedIpLinks.get(Pair.of(a, b))) {
						A_vlink_eip.set(e.getIndexInIlp(), ipLink2index.get(ipLink), 1.0); //LZ: initialize parameter A_vlink_eip with 1 to all ip links traversed by an vlink link
						WLightpathRequest lpr = ipLink.getCoupledLpRequest();
						for (WLightpath lp : lpr.getLightpaths()) {
							for(WFiber f : lp.getSeqFibers()) {
								A_vlink_f.set(e.getIndexInIlp(), fiber2index.get(f), 1.0);
								cap_fiber.set(fiber2index.get(f), SLOTS_FIBER*LINERATE);
							}
						}
					}
					latencyIpLinksMs = nodePair2WorstcaseLatencyMs.get(Pair.of(a, b)); //LZ: get IP links propagation latency 
				}
				latencyMs_vlink.set(e.getIndexInIlp(), latencyIpLinksMs); //total latency associated to the IP links and the end VNF
			}
			
			//LZ: uplink and downlink processing latency for node n
			final DoubleMatrix2D latencyMs_scr_n = DoubleFactory2D.sparse.make (D,N);
			
			//LZ: initialize processing latency per node, which depends on the number of servers available and on the VNF processing time
			for(WNode n : wNet.getNodes()) {
				for(WServiceChainRequest s : wNet.getServiceChainRequests()){
					double processingTime;
					double n_servers = n.getTotalNumCpus()/8; //LZ: 1 server contains 8 CPUs
					 //LZ: VNF processing time from excel
					WVnfType vnf = wNet.getVnfType(s.getSequenceVnfTypes().get(0)).get();
					processingTime = vnf.getProcessingTime_ms();
					latencyMs_scr_n.set(scr2index.get(s), wnode2index.get(n), processingTime/n_servers);
				}
			}
			
			final DoubleMatrix1D AcpuPerGbps_vn = DoubleFactory1D.dense.make (NUMVNS);
			final DoubleMatrix1D latencyMs_vn = DoubleFactory1D.dense.make(NUMVNS);
			for(String t : type2vns.keySet()) {
				if(t.contains(";")) continue;
				WVnfType vnf = wNet.getVnfType(t).get();
				for(VirtualNode vn : type2vns.get(t)) {
					AcpuPerGbps_vn.set(vn.getIndexInIlp(),vnf.getOccupCpu());
					latencyMs_vn.set(vn.getIndexInIlp(), vnf.getProcessingTime_ms());
				}
			}
			
			//LZ: propagation delay until DU
			final DoubleMatrix1D latencyMs_du = DoubleFactory1D.dense.make (D);
			for(WServiceChainRequest scr : wNet.getServiceChainRequests()) {
				WNode src = scr.getPotentiallyValidOrigins().first();
				WNode dst = scr.getPotentiallyValidDestinations().first();
				latencyMs_du.set(scr2index.get(scr), Double.valueOf(src.getArbitraryParamString())/200 + Double.valueOf(dst.getArbitraryParamString())/200);
			}
			
			//LZ: matrix indicating to which node and virtual node is associated
			final DoubleMatrix2D A_vn_n = DoubleFactory2D.sparse.make(NUMVNS,N);
			for(VirtualNode vn:vns) {
				if(vn.isAnycastDestination()) continue;
				if(vn.isAnycastOrigin()) continue;
				WNode n = vn.getNode();
				A_vn_n.set(vn.getIndexInIlp(), wnode2index.get(n), 1.0);
			}
			
			op.setInputParameter("idlePowerServer", idlePowerServer);
			op.setInputParameter("diffPowerServer", maxPowerServer - idlePowerServer);
			op.setInputParameter("capexServer", capexServer);
			op.setInputParameter("cpu_f", nfvTypesIncluding_cpuRamHdLat.stream().map(e->e.getSecond()).collect (Collectors.toList()) , "row"); /* for each NFV type, its CPU */
			op.setInputParameter("cpu_n", wNet.getNodes().stream().map(e->e.getTotalNumCpus()).collect(Collectors.toList()) , "row"); /* for each node, CPU capacity  */
			op.setInputParameter("powerTransponder", powerTransponder);
			op.setInputParameter("capexTransponder", capexTransponder);
			op.setInputParameter("slot_fiber", SLOTS_FIBER);
			
			/* Write the problem formulations */
			/* Forbidden SCR-VLINKs: For each SCR only some */
			/* acceptable_scr_vlink: 1 the SCR scr could traverse the vlink, 0 otherwise */
			final DoubleMatrixND acceptable_scr_vlink = new DoubleMatrixND(new int [] {D ,  NUMVLINKS} , "sparse");
	
			/* expFactor_scr_vlink: The factor that multiplies the Gbps offered in SCR, to have the Gbps in VLINK, WHEN the SCR traverses VLINK */
			final DoubleMatrixND expFactor_scr_vlink = new DoubleMatrixND(new int [] {D ,  NUMVLINKS} , "sparse");
			
			/* acceptable_scr_vn: 1 the SCR scr could be placed in node vn, 0 otherwise */
			final DoubleMatrixND acceptable_scr_vn = new DoubleMatrixND(new int [] {D ,  NUMVNS} , "sparse");	
			final DoubleMatrixND acceptable_scr_n = new DoubleMatrixND(new int [] {D ,  N} , "sparse");	
			
			
			for (WServiceChainRequest scr : wNet.getServiceChainRequests()){
				final int indexScr = scr2index.get(scr);
				/* From anycast origin to all its potential initial nodes (extended VNF type "SCRSTART" == ";") */
				for (VirtualNode vn : type2vns.get(";")) { //LZ: anycast origin VNF
					if (vn.getNode() == null) continue; //LZ: anycast origin or destination
					if (!scr.getPotentiallyValidOrigins().contains(vn.getNode())) continue; //LZ: if virtual node is not part of the set of valid origins of service chain, go to next virtual node
					final VirtualLink link = mapAnPair2VirtualLink.get(Pair.of(anycastOrigin, vn)); //LZ: get link from anycast origin to virtual node
					assert link != null;
					acceptable_scr_vlink.set(new int [] { indexScr, link.getIndexInIlp() }, 1.0);
					expFactor_scr_vlink.set(new int [] { indexScr, link.getIndexInIlp() }, 1.0);
				}
				/* From all its potential end nodes (extended VNF type "SCREND" == ";;") to anycast destination */
				for (VirtualNode vn : type2vns.get(";;")) { //LZ: anycast destination VNF
					if (vn.getNode() == null) continue; //LZ: anycast origin or destination
					if (!scr.getPotentiallyValidDestinations().contains(vn.getNode())) continue; //LZ: if virtual node is not part of the set of valid origins of service chain, go to next virtual node
					final VirtualLink link = mapAnPair2VirtualLink.get(Pair.of(vn , anycastDestination)); //LZ: get link from virtual node to anycast destination 
					assert link != null;
					acceptable_scr_vlink.set(new int [] { indexScr, link.getIndexInIlp() }, 1.0); 
					expFactor_scr_vlink.set(new int [] { indexScr, link.getIndexInIlp() }, 1.0);
				}
				/* Only those acceptable type-type pairs that are acceptable */
				final List<String> trueTravTypes = new ArrayList<> (scr.getSequenceVnfTypes());
				for (int indexTrueTypeToTraverse = 0 ; indexTrueTypeToTraverse < trueTravTypes.size() + 1 ; indexTrueTypeToTraverse ++) {
					final String previousExtendedTypeToTraverse = indexTrueTypeToTraverse == 0? ";" : trueTravTypes.get(indexTrueTypeToTraverse-1);
					final String thisExtendedTypeToTraverse = indexTrueTypeToTraverse == trueTravTypes.size()? ";;" : trueTravTypes.get(indexTrueTypeToTraverse);
					final double expansionFactor = indexTrueTypeToTraverse == 0? 1.0 : scr.getDefaultSequenceOfExpansionFactorsRespectToInjection().get(indexTrueTypeToTraverse-1);
					for (VirtualNode previousVn : type2vns.get(previousExtendedTypeToTraverse)) {
						for (VirtualNode thisVn : type2vns.get(thisExtendedTypeToTraverse)) {
							final VirtualLink link = mapAnPair2VirtualLink.get(Pair.of(previousVn , thisVn));
							assert link != null;
							acceptable_scr_vlink.set(new int [] {indexScr, link.getIndexInIlp()}, 1.0);
							expFactor_scr_vlink.set(new int [] {indexScr, link.getIndexInIlp()}, expansionFactor);
							if(indexTrueTypeToTraverse == 0) {
								acceptable_scr_vn.set(new int [] {indexScr, thisVn.getIndexInIlp()}, 1.0);
								acceptable_scr_n.set(new int [] {indexScr, wnode2index.get(thisVn.getNode())}, 1.0);
							}
						}
					}
				}
			}
			
			op.addDecisionVariable("xx_scr_vlink", true , new int[] { D, NUMVLINKS}, 
					new DoubleMatrixND (new int [] {D , NUMVLINKS}, "sparse"),acceptable_scr_vlink); //LZ: virtual links through which a service chain request passes
			op.addDecisionVariable("xx_scr_vlink1", true , new int[] { D, NUMVLINKS}, 
					new DoubleMatrixND (new int [] {D , NUMVLINKS}, "sparse"),acceptable_scr_vlink); //LZ: virtual links from source node to vnf node through which a service chain request passes 
			op.addDecisionVariable("xx_scr_vlink2", true , new int[] { D, NUMVLINKS}, 
					new DoubleMatrixND (new int [] {D , NUMVLINKS}, "sparse"),acceptable_scr_vlink); //LZ: virtual links from vnf node to dst though which a service chain request passes
			op.addDecisionVariable("xx_scr_vn", true , new int[] { D, NUMVNS }, 
					new DoubleMatrixND (new int [] {D , NUMVNS}, "sparse"),acceptable_scr_vn); // LZ: virtual nodes through which scr passes
			op.addDecisionVariable("xx_scr_n", true , new int[] { D, N }, 
					new DoubleMatrixND (new int [] {D , N}, "sparse"),acceptable_scr_n); //LZ: nodes through which scr passes
			op.addDecisionVariable("xx_n" , true , new int [] { 1, N } , 0 , 1); //LZ: indicates whether a node is used
			op.addDecisionVariable("procLatency_n", false, new int[] { D, N }, 0.0, 10.0); //LZ: processing latency
			
			op.setInputParameter("traf_scr", index2scr.stream().map(e->e.getCurrentOfferedTrafficInGbps()).collect(Collectors.toList()) , "row"); //LZ: current carried traffic by service chain request
			op.setInputParameter("maxlatencyMs_du", index2scr.stream().map(e->e.getMaxLatencyFromOriginEndNode_ms()).collect(Collectors.toList())  , "row"); //LZ: maximum end-to-end latency
			op.setInputParameter("maxLatencySplitMs_scr", index2scr.stream().map(e->e.getListMaxLatencyFromOriginToVnStartAndToEndNode_ms().get(0)).collect(Collectors.toList())  , "row"); //LZ: maximum latency source-destination allowed by service chain request
			op.setInputParameter("cap_fiber", cap_fiber, "row"); //LZ: link maximum capacity
			op.setInputParameter("cap_transp", cap_fiber, "row");
			op.setInputParameter("latencyMs_vlink", latencyMs_vlink , "row"); //LZ: maximum latency to traverse vlink (propagation + vnf)
			op.setInputParameter("latencyMs_du", latencyMs_du, "row"); //LZ: DU propagation delay
			op.setInputParameter("latencyMs_vn", latencyMs_vn, "row"); //LZ: processing latency
			op.setInputParameter("A_vn_scr", A_vn_scr); /* 1 in position (n,e) if service chain request starts in n, -1 if it ends in n, 0 otherwise */
			op.setInputParameter("A_vn_scrOrig", A_vn_scrOrig); /* 1 in position (n,e) if service chain request starts in n, -1 if it ends in n, 0 otherwise */
			op.setInputParameter("A_vn_scrDest", A_vn_scrDest); /* 1 in position (n,e) if service chain request starts in n, -1 if it ends in n, 0 otherwise */
			op.setInputParameter("A_vn_vlink", A_vn_vlink); /* 1 in position (n,d) if vlink in n, -1 if it ends in n, 0 otherwise */
			op.setInputParameter("P_vn_vlink", P_vn_vlink); //LZ: 1 in position (n,d) if vlink destination virtual node n can be assigned to a vnf  
			op.setInputParameter("A_vn_n", A_vn_n); //LZ: 1 in position (vn,n) if virtual node vn is assigned to node n 
			op.setInputParameter("A_vlink_eip", A_vlink_eip); /* Fraction of traffic of vlink traversing ip link eipLink (according to OSPF) */
			op.setInputParameter("A_vlink_f", A_vlink_f); //LZ: vlink assigned to fiber
			op.setInputParameter("AcpuPerGbps_vn", AcpuPerGbps_vn,"row"); /* Amount of CPU consumed in node n per GBps in vlink *
			
			double alfa = 0.5;
			double beta = 1 - alfa;
			op.setInputParameter("alfa", alfa); //LZ: weight of active nodes minimization 
			op.setInputParameter("beta", beta); //LZ: weight of bandwidth minimization
			
			/*********************************/
			/****** Objective function *******/
			/*********************************/
			
			switch (optimizationType.getString()){
				//LZ: set of objective functions
				case "ILP": 
					//LZ: minimization of nodes and transponder power consumption
					op.setObjectiveFunction("minimize", " alfa * ( (idlePowerServer * (sum (xx_n))) + "
							//+ "(diffPowerServer * sum ((traf_scr * (xx_scr_vlink .* expFactor_scr_vlink)) * AcpuPerGbps_vlink_n))) + "
							+ "(diffPowerServer * sum ((((traf_scr * xx_scr_vn) .* AcpuPerGbps_vn) * A_vn_n))) ) + "
							+ "beta * ( 2 * powerTransponder * sum ( sum( xx_scr_vlink * A_vlink_f ) ) )");
					break;
				case "ILP_nodes": 
					//LZ: minimization of number of nodes and of number of used links 
					System.out.println("ILP_nodes");
					op.setObjectiveFunction("minimize", " alfa * ( sum (xx_n) )");
					//op.setObjectiveFunction("minimize", " alfa * ( sum (xx_n) ) + beta * (sum ((traf_scr * (xx_scr_vlink .* expFactor_scr_vlink)) * A_vlink_eip))");
					break;
				default: 
					//LZ: minimization of the number of used links
					op.setObjectiveFunction("minimize", "sum ((traf_scr * (xx_scr_vlink .* expFactor_scr_vlink)) * A_vlink_eip)");
					break;
			}
			
			/*********************************/
			/********** Constraints **********/
			/*********************************/
			
			/*** routing constraints ***/
			//LZ: path from origin to destination is composed by a path from origin to VNF node and another from VNF node to destination
			op.addConstraint("(xx_scr_vlink1 + xx_scr_vlink2) == xx_scr_vlink");
			//LZ: all service chain requests are carried
			op.addConstraint("A_vn_vlink * (xx_scr_vlink') == A_vn_scr");
			
			//LZ: a request from/to DU d to/from the gateway passes in the virtual node processing its demand
			op.addConstraint("xx_scr_vlink * P_vn_vlink == xx_scr_vn");
			//LZ: assignment of a virtual node to a physical node 
			op.addConstraint("xx_scr_vn * A_vn_n == xx_scr_n");
			
			//LZ: all service chains are carried from origin to the virtual node with the VNF and then to the destination
			op.addConstraint("A_vn_vlink * (xx_scr_vlink1') == A_vn_scrOrig - xx_scr_vn'");
			op.addConstraint("A_vn_vlink * (xx_scr_vlink2') == A_vn_scrDest + xx_scr_vn'");
			
			//LZ: each virtual network function is assigned to a single node
			for(int d = 0; d < D; d++) {
				op.setInputParameter("d", d);
				op.addConstraint("sum (xx_scr_n(d,all)) == 1"); 
			}
			
			//LZ: node is active if and only if it is assigned to at least one DU
			for(int n = 0; n < N; n++) {
				op.setInputParameter("M", Integer.MAX_VALUE);
				op.setInputParameter("n", n);
				op.addConstraint("M * xx_n(all,n) >= sum(xx_scr_n(all,n))");
				op.addConstraint("xx_n(all,n) <= sum(xx_scr_n(all,n))");
			}
			
			//LZ: if D-RAN scenario, each node processes the traffic of the DUs directly connected to it
			if(optimizationType.getString().contains("DRAN")) {
				for(int n = 0; n < N; n++) {
					if(index2wnode.get(n).equals(wNet.getNodesConnectedToCore().first())) continue;
					op.setInputParameter("n", n);
					for(int d = 0; d < D; d++) {
						op.setInputParameter("d", d);
						WServiceChainRequest demand = index2scr.get(d);
						WNode org = demand.getPotentiallyValidOrigins().first();
						WNode dst = demand.getPotentiallyValidDestinations().first();
						if(wnode2index.get(org).equals(n)) {
							op.addConstraint("xx_scr_n(d,n) == 1");
						}
						else if(wnode2index.get(dst).equals(n)) {
							op.addConstraint("xx_scr_n(d,n) == 1");
						}
						else{
							op.addConstraint("xx_scr_n(d,n) == 0");
						}
					}
				}
			}
							
			/*** capacity constraints ***/
			op.addConstraint("(traf_scr * (xx_scr_vlink .* expFactor_scr_vlink)) * A_vlink_f <= cap_fiber "); /* Enough capacity in the IP links */
			if(optimizationType.getString().contains("ILP")) {
				op.addConstraint("((traf_scr * xx_scr_vn) .* AcpuPerGbps_vn) * A_vn_n <= cpu_n "); /* Enough CPUs */
			}
			
			/*** latency constraints ***/
			if(optimizationType.getString().contains("ILP")) {
				DoubleMatrix1D maxLat = DoubleFactory1D.dense.make (N);
				for(int i = 0; i < N; i++) maxLat.set(i, 10.0);
				DoubleMatrix1D minLat = DoubleFactory1D.dense.make (N);
				for(int i = 0; i < N; i++) minLat.set(i, 0.0);
				DoubleMatrix1D onesLat = DoubleFactory1D.dense.make (N);
				for(int i = 0; i < N; i++) onesLat.set(i, 1.0);
				
				//x = xx_scr_n
				//y = (traf_scr * xx_scr_n) .* latencyMs_n_Xl 
				for(int d = 0; d < D; d = d + 2) {
					op.setInputParameter("d_ul", d);
					op.setInputParameter("d_dl", d + 1);
					
					op.setInputParameter("maxLat", maxLat, "row");
					op.setInputParameter("minLat", minLat, "row");
					op.setInputParameter("onesLat", onesLat, "row");
					//LZ: 0*x <= z <= 1.5x
					//LZ: uplink
					op.addConstraint("procLatency_n(d_ul,all) <= maxLat .* xx_scr_n(d_ul,all) ");
					op.addConstraint("procLatency_n(d_ul,all) >= minLat");
					//LZ: downlink
					op.addConstraint("procLatency_n(d_dl,all) <= maxLat .* xx_scr_n(d_dl,all) ");
					op.addConstraint("procLatency_n(d_dl,all) >= minLat");
					
					//LZ: latency links to traverse + latency BS to first node + processing latency
					//LZ: y - 1.5 (1 - x) <= z <= y - 0(1 - x)
					//LZ: uplink
					op.addConstraint("procLatency_n(d_ul,all) <= ((traf_scr * xx_scr_vn) .* latencyMs_vn) * A_vn_n");
					op.addConstraint("procLatency_n(d_ul,all) >= ((traf_scr * xx_scr_vn) .* latencyMs_vn) * A_vn_n - maxLat .* (onesLat - xx_scr_n(d_ul,all))");
					//LZ: downlink
					op.addConstraint("procLatency_n(d_dl,all) <= ((traf_scr * xx_scr_vn) .* latencyMs_vn) * A_vn_n");
					op.addConstraint("procLatency_n(d_dl,all) >= ((traf_scr * xx_scr_vn) .* latencyMs_vn) * A_vn_n - maxLat .* (onesLat - xx_scr_n(d_dl,all))"); 
					
					op.addConstraint("(xx_scr_vlink1(d_ul,all) * latencyMs_vlink') + latencyMs_du(d_ul) + sum(procLatency_n(d_ul,all)) <= maxLatencySplitMs_scr(d_ul) "); // VNF latency respected in all the service chain requests
					op.addConstraint("(xx_scr_vlink2(d_dl,all) * latencyMs_vlink') + latencyMs_du(d_dl) + sum(procLatency_n(d_dl,all)) <= maxLatencySplitMs_scr(d_dl) "); // VNF latency respected in all the service chain requests
					
					//LZ: End-to-end latency respected in all the service chain requests
					op.addConstraint("xx_scr_vlink(d_ul,all) * latencyMs_vlink' + "
							+ " xx_scr_vlink(d_dl,all) * latencyMs_vlink' + "
							+ " latencyMs_du(d_ul) + latencyMs_du(d_dl) + "
							+ " sum(procLatency_n(d_ul,all)) + "
							+ " sum(procLatency_n(d_dl,all)) <= maxlatencyMs_du(d_ul)' ");  
				}
			}

					
			/*********************************/
			/************* Solve *************/
			/*********************************/
			System.out.println("solving");
			op.solve("cplex", "solverLibraryName", "" , "maxSolverTimeInSeconds" , -1);
		
			/* If no solution is found, quit */
			if (op.feasibleSolutionDoesNotExist()) throw new Net2PlanException("The problem has no feasible solution");
			if (!op.solutionIsFeasible()) throw new Net2PlanException("A feasible solution was not found");
			
			/*************************************************************************************/
			/******************** CREATE THE OUTPUT DESIGN FROM THE SOLUTION *********************/
			/*************************************************************************************/
			final DoubleMatrix2D xx_scr_vlink = op.getPrimalSolution("xx_scr_vlink").view2D();
			
			final Function<Collection<VirtualLink> , List<? extends WAbstractNetworkElement>> travIpLinksAndVnfs = vLinks -> {
				final List<WAbstractNetworkElement> res = new ArrayList<> ();
				VirtualNode currentNode = anycastOrigin;
				final Set<VirtualLink> traversedVirtualLinks = new HashSet<> ();
				while (true) {
					final VirtualNode initialNode = currentNode;
					assert vLinks.stream().filter(e->e.getA().equals (initialNode)).count() == 1;
					final VirtualLink nextVirtual = vLinks.stream().filter(e->e.getA().equals (initialNode)).findFirst().orElse(null);
					assert nextVirtual != null;
					assert !traversedVirtualLinks.contains(nextVirtual); traversedVirtualLinks.add(nextVirtual);
					final VirtualNode endNode = nextVirtual.getB();
					if (initialNode.equals(anycastOrigin)) { 
						currentNode = endNode; 
						continue;
					}
					if (endNode.equals(anycastDestination)) break;
					final List<WIpLink> travIpLinks = nodePair2TraversedIpLinks.get (Pair.of(nextVirtual.getA ().getNode() , nextVirtual.getB ().getNode()));
					res.addAll(travIpLinks);
					final String endExtendedVnf = nfvTypesIncluding_cpuRamHdLat.get(nextVirtual.getB().getIndexType()).getFirst(); 
					assert !endExtendedVnf.equals(";");
					if (!endExtendedVnf.equals(";;"))
					{
						final SortedSet<WVnfInstance> vnf = nextVirtual.getB().getNode().getVnfInstances(endExtendedVnf);
						assert vnf.size() == 1;
						res.add(vnf.first());
					}
					currentNode = nextVirtual.getB();
				}
				return res;
			};
	
			/* Create all the VNF instances, with no capacity, 0 CPU etc. Only set the processing latency  */
			for (WNode n : wNet.getNodes()){
				for (Quintuple<String,Double,Double,Double,Double> vnfType : nfvTypesIncluding_cpuRamHdLat){
					if (vnfType.getFirst().equals(";") || vnfType.getFirst().equals(";;")) continue;
	 				wNet.addVnfInstance(n, "", vnfType.getFirst() , 0.0 , 0.0 , 0.0 , 0.0 , vnfType.getFifth());
				}
			}
			
			/* Set the paths of all the SCRs */
			for (int indexScr = 0 ; indexScr < D ; indexScr ++){
				final WServiceChainRequest scr = index2scr.get(indexScr);
				final List<VirtualLink> unorderedVirtualTraversed = new ArrayList<> ();
				for (int vlink = 0; vlink < NUMVLINKS ; vlink ++)
					if (xx_scr_vlink.get(indexScr, vlink) > Configuration.precisionFactor) 
						unorderedVirtualTraversed.add(index2vlink.get(vlink));
				final double traf = scr.getCurrentOfferedTrafficInGbps();
				//final double traf = scr.getTrafficIntensityInfo(1).get().getSecond();
				List<? extends WAbstractNetworkElement> ipLinksVnfs = travIpLinksAndVnfs.apply(unorderedVirtualTraversed);
				scr.addServiceChain(ipLinksVnfs, traf);
			}
	
			/* The VNF instance capacities, CPUs, RAM, HD are set according to its actual traffic. */
			for (WVnfInstance vnf : new ArrayList<> (wNet.getVnfInstances())){
				double n_server = vnf.getHostingNode().getTotalNumCpus()/8;
				//System.out.println(vnf.getProcessingTimeInMs() + " " + vnf.getVnfType().get().getProcessingTime_ms()*vnf.getOccupiedCapacityInGbps());
				double traf = 0;
				for(WServiceChainRequest sc : vnf.getTraversingServiceChainRequests()) {
					traf = traf + sc.getCurrentCarriedTrafficGbps();
				}
				double newProcessingTime = vnf.getProcessingTimeInMs() + vnf.getVnfType().get().getProcessingTime_ms()*traf/n_server;
				vnf.setProcessingTimeInMs(newProcessingTime);
				
				final Quintuple<String,Double,Double,Double,Double> info = nfvTypesIncluding_cpuRamHdLat.get(getTypeIndex.apply(vnf.getType()));
				final double trafProcessedGbps = vnf.getOccupiedCapacityInGbps();
				if (trafProcessedGbps < Configuration.precisionFactor) { 
					vnf.remove();
				}
				else {
					vnf.setCapacityInGbpsOfInputTraffic(Optional.of (trafProcessedGbps), 
						Optional.of (trafProcessedGbps * info.getSecond()), 
						Optional.of (trafProcessedGbps * info.getThird()), 
						Optional.of (trafProcessedGbps * info.getFourth()));
				}
			}
			
			/*************************************************************************************/
			/***************************      CHECK THE SOLUTION      ****************************/
			/*************************************************************************************/
			assert wNet.getServiceChainRequests().stream().allMatch(e->e.getCurrentOfferedTrafficInGbps()> Configuration.precisionFactor);
			assert wNet.getServiceChainRequests().stream().allMatch(e->e.getCurrentBlockedTraffic() == 0);
			assert wNet.getServiceChainRequests().stream().allMatch(e->e.getServiceChains().size() == 1);
			assert wNet.getVnfInstances().stream().allMatch(e->e.getOccupiedCapacityInGbps() <= e.getCurrentCapacityInGbps() + Configuration.precisionFactor);
			for (WNode n : wNet.getNodes())
			{
				assert n.getTotalNumCpus() >= n.getVnfInstances().stream().mapToDouble(e->e.getOccupiedCpus()).sum() + Configuration.precisionFactor;
				assert n.getTotalRamGB() >= n.getVnfInstances().stream().mapToDouble(e->e.getOccupiedRamInGB()).sum() + Configuration.precisionFactor;
				assert n.getTotalHdGB() >= n.getVnfInstances().stream().mapToDouble(e->e.getOccupiedHdInGB()).sum() + Configuration.precisionFactor;
			}
			assert wNet.getIpLinks().stream().allMatch(e->e.getCarriedTrafficGbps() <= e.getCurrentCapacityGbps() + Configuration.precisionFactor);


		//return "Ok!: The solution found is guaranteed to be optimal: " + op.solutionIsOptimal();
		return "OK";
	}

	private static Pair<Map<Pair<WNode,WNode> , List<WIpLink>> , Map<Pair<WNode,WNode> , Double>> getPotentialIpRoutingNormalizedCarrideTrafficAndMaxLatencyMsNoFailureState_shortestPath (WNet net , boolean trueIsMinimLatencyFalseMinimHops)
	{
		assert net.getIpUnicastDemands().isEmpty();
		final Map<Pair<WNode,WNode>,List<WIpLink>> fractionOfTrafficFromOspf = new HashMap<> ();
		final Map<Pair<WNode,WNode>,Double> worstCaseLatencyMs = new HashMap<> ();
		final Map<WIpLink,Double> costMap = new HashMap<> ();
		//LZ: associate cost (latency or #hops) to each link
		for (WIpLink e : net.getIpLinks()) costMap.put(e, trueIsMinimLatencyFalseMinimHops? e.getWorstCasePropagationDelayInMs() : 1.0);
				
		for (WNode n1 : net.getNodes())
		{
			for (WNode n2 : net.getNodes())
			{
				if (n1.equals(n2)) continue;
				//LZ: get shortest IP path from node 1 to node 2 using cost (latency or #hops) as cost function
				final List<List<WIpLink>> kPaths = net.getKShortestIpUnicastPath(1, net.getNodes(), net.getIpLinks(), n1, n2, Optional.of(costMap));
				if (kPaths.isEmpty()) throw new Net2PlanException ("The IP toplology is not connected");
				fractionOfTrafficFromOspf.put(Pair.of(n1, n2), kPaths.get(0));//LZ: add calculated path
				worstCaseLatencyMs.put(Pair.of(n1, n2), kPaths.get(0).stream().mapToDouble(e->e.getWorstCasePropagationDelayInMs()).sum());//LZ: add total propagation delay of calculated path
			}
			fractionOfTrafficFromOspf.put(Pair.of(n1, n1), new ArrayList<> ());//LZ: add path to the same node
			worstCaseLatencyMs.put(Pair.of(n1, n1), 0.0);
		}
		return Pair.of (fractionOfTrafficFromOspf , worstCaseLatencyMs );
	}

	@Override
	public String getDescription()
	{
		return "Algorithm for NFV placement";
	}
	
	@Override
	public List<Triple<String, String, String>> getParameters()
	{
		/* Returns the parameter information for all the InputParameter objects defined in this object (uses Java reflection) */
		return InputParameter.getInformationAllInputParameterFieldsOfObject(this);
	}
	
	static class VirtualNode
	{
		final WNode node; final int indexType; final int indexInIlp; final boolean isAnycastOrigin; final boolean isAnycastDestination;
		VirtualNode (WNode n , int indexType , int indexInLp) { this (n , indexType , indexInLp , false , false); }
		VirtualNode (WNode n , int indexType , int indexInLp , boolean isAnycastOrigin, boolean isAnycastDestination) { this.node = n; this.indexType = indexType; this.indexInIlp = indexInLp; this.isAnycastOrigin = isAnycastOrigin; this.isAnycastDestination = isAnycastDestination; }
		VirtualNode (boolean isOrigin , int indexInLp) { this(null, -1 , indexInLp , isOrigin , !isOrigin); }
		public int getIndexInIlp () { return indexInIlp; }
		public int getIndexType () { return indexType; }
		public boolean isAnycastOrigin () { return this.isAnycastOrigin; }
		public boolean isAnycastDestination () { return this.isAnycastDestination; }
		public WNode getNode () { return node; }
		public String toString () { return isAnycastOrigin? "AnycastOrigin" : isAnycastDestination? "AnycastDest" : getNode().toString(); }
	};
	static class VirtualLink
	{
		final VirtualNode a; final VirtualNode b; final int indexInIlp; 
		VirtualLink (VirtualNode a , VirtualNode b , int indexInIlp) { this.a = a ; this.b = b ; this.indexInIlp = indexInIlp; }
		public int getIndexInIlp () { return indexInIlp; }
		public VirtualNode getA () { return a; }
		public VirtualNode getB () { return b; }
		public String toString () { return getA().toString() + "->"+ getB().toString(); }
	};

	private static <T> Function<T,Integer> createMapIndex (Collection<T> list) 
	{
		final Map<T,Integer> res = new HashMap<> ();
		for (T e : list) res.put(e , res.size());
		return e->{ final Integer index = res.get(e); return index; };
	}
	
	public class ElementComparator implements Comparator<WAbstractNetworkElement>{
		private Point2D posToCompare;
		public ElementComparator(WNode node) {
			this.posToCompare = node.getNodePositionXY();
		}
		public ElementComparator() {}
		
		@Override
		public int compare(WAbstractNetworkElement e1, WAbstractNetworkElement e2) {
			int result = 0;
			if(e1.isNode()) {
				Point2D pos1 = e1.getAsNode().getNodePositionXY();
				Point2D pos2 = e2.getAsNode().getNodePositionXY();
				double dist1 = distance(pos1.getY(),pos1.getX(),posToCompare.getY(),posToCompare.getX(),"K");
				double dist2 = distance(pos2.getY(),pos2.getX(),posToCompare.getY(),posToCompare.getX(),"K");
				if (dist1 > dist2) result = 1;
				if (dist1 < dist2) result = -1;
			}
			if(e1.isLightpathRequest()) {
				WLightpathRequest lpr1 = e1.getAsLightpathRequest();
				WLightpathRequest lpr2 = e2.getAsLightpathRequest();
				Point2D pos1 = lpr1.getA().getNodePositionXY();
				Point2D pos2 = lpr1.getB().getNodePositionXY();
				double dist1 = distance(pos1.getY(),pos1.getX(),pos2.getY(),pos2.getX(),"K");
				pos1 = lpr2.getA().getNodePositionXY();
				pos2 = lpr2.getB().getNodePositionXY();
				double dist2 = distance(pos1.getY(),pos1.getX(),pos2.getY(),pos2.getX(),"K");
				if (dist1 > dist2) result = 1;
				if (dist1 < dist2) result = -1;
			}
			return result;
		}
		
		private double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
			if ((lat1 == lat2) && (lon1 == lon2)) {
				return 0;
			}
			else {
				double theta = lon1 - lon2;
				double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
				dist = Math.acos(dist);
				dist = Math.toDegrees(dist);
				dist = dist * 60 * 1.1515;
				if (unit.equals("K")) {
					dist = dist * 1.609344;
				} else if (unit.equals("N")) {
					dist = dist * 0.8684;
				}
				return (dist);
			}
		}
	}
}